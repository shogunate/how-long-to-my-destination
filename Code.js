// From:				    (Associated sheet looks like this)
// Perttulantie 4		<- Input A2		

//      	Transit	Bicycle	Car	    Walk
// Kamppi	15 mins	12 mins	9 mins	44 mins     <- minutes are filled in as output from the code (top/side headers are fixed)
// Laru	  7 mins	2 mins	3 mins	7 mins
// WorkA 	25 mins	23 mins	12 mins	1 hour 19 mins
// WorkB	34 mins	39 mins	15 mins	2 hours 6 mins


function onEdit(e) {
  var sh = e.source.getSheets()[0]; //
  var range = e.range.getA1Notation();

  if (range === "A2") {
    var from = e.value;//sh.getRange("A2").getValue();
    var out = getDirections(from);

    if (out) {
      sh.getRange(5, 2, 4, 4).setValues(out);
      //Copy table to the right side
      var table = sh.getRange(4, 1, 5, 5).getValues();
      sh.getRange(sh.getLastRow() + 2, 8, 5, 5).setValues(table);
    }
    else {
      Logger.log("From value is " + from + ". Process failed");
    }
  }
}

function getDirections(from = 'Ramón y cajal 5') {
  var destinations = ['Kamppi', 'Pajalahdentie 10', 'Metsänpojankuja 1', 'Esikunnankatu 2'];
  var modes = ['TRANSIT', 'BICYCLING', 'DRIVING', 'WALKING'];
  var out = [];
  for (d of destinations) {
    var itinerary = Maps.newDirectionFinder()
      .setOrigin(from)
      .setDestination(d);
    if (itinerary) {
      var rowArr = [];
      var tempDir = {};
      for (m of modes) {
        tempDir = itinerary.setMode(Maps.DirectionFinder.Mode[m])
          .getDirections();
        if (tempDir.status !== "NOT_FOUND") {
          rowArr.push(tempDir.routes[0].legs[0].duration.text);
        }
        else {
          Browser.msgBox('Couldn\'t find an itinerary for that address');
          return null;
        }
      }
      out.push(rowArr);
    }
  }
  return out;
}

// function onOpen(){
//   SpreadsheetApp.getUi()
//   .createMenu('Admin')
//   .addItem('getTimes', 'printValues')
//   .addToUi();
// }

// function printValues(){
//   var sh = SpreadsheetApp.openById("1m7M5CWgO7bkBOw-f9CZkobcXwrq1P-zgw1i6078Z8XU").getSheets()[0];
//   var from = sh.getRange("A2").getValue();
//   var out = getDirections(from);
//   sh.getRange(5,2,4,4).setValues(out);
//   //Copy table to the right side
//   var table = sh.getRange(4,1,5,5).getValues();
//   sh.getRange(sh.getLastRow()+2,8,5,5).setValues(table);  
// }